# OHLOOM - Benutzerhilfe \[GER\]

[**\[ENG\]**](UserGuide.md)

![](User_Guide.jpg)


## Index

1. [Bereite Aufspannung vor](#prep-windup)
2. [Aufspannungswerkzeuge](#windup-utilities)
3. [Kette aufspannen](#wind-it-up)
4. [Wie webt man mit OHLOOM?](#how-to-weave)


## 1. Bereite Aufspannung vor{#prep-windup}

Um die Wolle beim Aufspannen zu schützen, benutzt man Pappstreifen, die nach jeweils 2/3 Umdrehung eingelegt werden. Die Streifen sollten 2 cm breit und 55-57 cm lang sein.

| | | |
| :---------: | :------------: | :---: |
| ![](PrepWindUp_1.jpg) 1. Zeichne Linien |  ![](PrepWindUp_2.jpg) 2. Benutze das Messer |  ![](PrepWindUp_3.jpg) 3. 10-20 Streifen |


## 2. Aufspannwerkzeuge {#windup-utilities}

| | |
| :---------: | :------------: |
| ![](WindUp_1.jpg) 2 x Grosse Klemmen, 1 Kleine Klemmen | ![](WindUp_2.jpg) 1 x Schere |
| ![](WindUp_3.jpg) 1 X Häkel-Nadel | ![](WindUp_4.jpg) 1 x Stab mit Fuss |


## 3. Spann die Kette auf {#wind-it-up}

| |
| :---------: |
| 1. Messe die Länge des Projektes ab (z.B. Länge des Schals) |
| 2. Klemme den Stab auf der einen Seite mit der kleinen Klemme fest  ![](WindUp_4.jpg) |
| 3. Benutze die grossen Klemmen mit dem Webstuhl auf der anderen Seite!  Beachte dabei die Seite mit dem kürzeren Seite zum Kammhalter nach Aussen zu drehen!  ![](WindUp_5.jpg) |
| 4. Lass die Wolle auf dem Boden und führe den Faden zwischen der Welle  und dem Querholm und knote die Schnur und die Schnurleiste  ![](WindUp_6.jpg) |
| 5. Führe nun den Faden als Schlinge durch den ersten langen Schlitz des Kammes  und dann am anderen Ende um den Stab  ![](WindUp_7.jpg) |
| 6. Nehme anschliessend den Faden zu einer neuen Schlinge  und führe es über die Schnurleiste entlang, durch den nächsten langen Schlitz  und wieder um den Stab am anderen Ende  ![](WindUp_8.jpg) |
| 7. Verfahre mit allen langen Schlitzen auf diese Weise, wobei die neue Schlinge abwechselnd unter und über die Schnurleiste geführt wird  ![](WindUp_9.jpg) |
| 8. Nacheinander werden die Schlingen übereinander auf dem Pol platziert  ![](WindUp_10.jpg) |
| 9. Wenn man die Farbe ändern möchte, schneide den Faden durch  und verknote das lose Ende an der Schnurleiste, und verfahre weiter von 4.  ![](WindUp_19.jpg)![](WindUp_20.jpg)![](WindUp_21.jpg) |
| 10. Nachdem alle langen Schlitze (oder die Schlitze fuer die Breite des Projekts) mit Wolle belegt sind, benutze ein Stück Faden, um alle Schlingen vor dem Pol miteinander zu verknoten. Benutze eine Schleife, da es später wieder gelöst wird.  ![](WindUp_22.jpg)![](WindUp_23.jpg) |
| 11. Der nächste Schritt funktioniert am besten mit einer zweiten Person, welche den Bündel von Schlaufen vom Pol nimmt und zum Aufrollen straff hält. |
| 12. Positioniere den Kamm in der Leer Position vor dem Kammhalter.  ![](WindUp_24.jpg) |
| 13. Fuege die Pappstreifen ein, wenn die Wolle aufgerollt wird. Rolle bis die Schleife, die alle Schlingen zusammenhält, am Kamm angekommen ist.  ![](WindUp_25.jpg)![](WindUp_26.jpg) |
| 14. Schneide mit der Schere alle Schlingen durch.  ![](WindUp_27.jpg) |
| 15. Loese die Klemmen vom Webstuhl, drehe den Webstuhl und nehme davor Platz. Löse die Schleife.  ![](WindUp_28.jpg) |
| 16. Gehe durch jeden langen Schlitz, entnehme einen der beiden Fäden,  und benutze die Häkel-Nadel, um den Faden durch die kleine Öse links  neben dem Schlitz durchzufädeln.  ![](WindUp_29.jpg) |
| 17. Nachdem alle vorhandenen Fädenpaare auf lange und kurze Schlitze aufgeteilt wurden,  knote circa 10 Fäden zusammen.  ![](WindUp_30.jpg) |
| 18. Nehme einen Faden (ca. 2 m) und führe dessen Hälfte um die Schnurleiste  ![](WindUp_31.jpg) |
| 19. Jetzt wickle den Faden über die Schnurleiste und durch die jeweiligen Knoten. |
| 20. Halte den Aufwicklung straf, das nach der Verknotung am Ende die Fäden gestraft in der Waagerechten liegen.  ![](WindUp_32.jpg) |
| 21. Nimm das Schiff, die Wolle und wickle sie auf das Schiff, wie auf dem Foto gezeigt.  ![](WindUp_33.jpg)![](WindUp_34.jpg) |
| 22. Positioniere den Kamm in die obere oder untere Halterung des Kammhalters,  damit der nächste Schritt möglich wird. |
| 23. Nimm wieder ein Stueck Wolle und doppele es (siehe Foto).  Benutze da Schiff um den Doppelfaden durchzufaedeln. |
| 24. Ziehe den Kamm heran und positioniere ihn anschliessend in der entgegengesetzten Position des Kammhalters.  Fädele dann mit Hilfe des Schiffes den Doppelfaden erneut durch.  ![](WindUp_35.jpg) |
| 23. Wiederhole diese Prozedur einige Male! Der Doppelfaden trägt dazu bei,  dass die Abstände zwischen den Fäden im optimalem Fall gleich gross sind!  ![](WindUp_36.jpg) |


### Gratulation! Der Webstuhl ist nun startklar!

## 4. Wie webt man mit OHLOOM? {#how-to-weave}

| |
| :---------: |
| 1. Positioniere den Kamm in die oberen Haltung, und fädele das Schiff durch.  ![](WindUp_37.jpg) |
| 2. Halte den Faden am anderem Ende vom Schiff mit Zeigefinger und Daumen, sodass es nicht zu weit zusammengezogen wird. Lass den Faden in diagonaler Position.  ![](WindUp_38.jpg) |
| 3. Ziehe den Kamm und damit den Faden heran und wiederhole das Heranziehen einige Male, sodass der Faden keine Lücken zum vorherigem Fadenlauf aufzeigt.  ![](WindUp_39.jpg) |
| 4. Nun positioniere den Kamm in der unteren Haltung.  ![](WindUp_40.jpg) |
| 5. Bewege das Schiff nun von der andere Seite (links oder rechts) durch, halte Faden am Ende des bereits gewebten Teil beim Durchziehen, ziehe den Kamm mehrere Male heran und verfahre weiter wie 1.  ![](WindUp_41.jpg) |


### Und so weiter und so weiter ... Viel Glück und Spass mit den Projekten!
