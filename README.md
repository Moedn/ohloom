# OHLOOM

**O**pen **H**ardware **Loom**
(GER: Webstuhl)

You may want to see [the original project Wiki](
https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM),
though this repo practically makes the Wiki version obsolete,
as it contains improvements,
and further development will happen here.

## Guides

| [User Guide](Documentation/User_Guide/UserGuide.md) | [Assembly Guide](Documentation/Assembly_Guide/AssemblyGuide.md) |
| :----: | :----: |
| [![](Documentation/User_Guide/User_Guide.jpg)](Documentation/User_Guide/UserGuide.md) | [![](Documentation/Assembly_Guide/Assembly_Guide.jpg)](Documentation/Assembly_Guide/AssemblyGuide.md) |
| [ENG](Documentation/User_Guide/UserGuide.md) / [GER](Documentation/User_Guide/UserGuide_de.md) | [ENG](Documentation/Assembly_Guide/AssemblyGuide.md) / [GER](Documentation/Assembly_Guide/AssemblyGuide_de.md) |

## Plans for the Parts

The 3D plans for the parts of the loom are available as source files in the OpenSCAD format.
Alongside you will find read-only versions as generated STL files
and sometimes technical drawings as PDFs.

You will find all these in this repository in the folders
[3DParts/](./3DParts/) and
[WoodParts/](./WoodParts/).
