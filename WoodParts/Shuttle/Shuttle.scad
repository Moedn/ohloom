/*File Info--------------------------------------------------------------------
File Name: Shuttle.scad
Project Name: OpenHardware LOOM - OHLOOM
License: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 
Name: Jens Meisner
Date: 08/01/20
Desc:  This file is part of the OHLOOM Project. Original design by Oliver Slueter, who made all wooden parts without a CNC Router. https://wiki.opensourceecology.de/Open_Hardware-Webstuhl_%E2%80%93_OHLOOM
Usage: 
./OHLoom_Documentation/Assembly_Guide/AssemblyGuide.md
./OHLoom_Documentation/User_Guide/OHLOOM_UserGuide.md
/*
/*Modifications------------------------------------------------------------
New File Name: Enter name, if it has changed
Name: Enter name of author
Date: 
Desc: Describe changes of the original design
*/
//Please continue with any fur enter any further modification here
//--------------------------------------------------------------------------------


length=500;
width=45;
height=6;
hole_d=20;
shaft_l=hole_d*2;
shaft_w=15;
export_dxf=false;


if(export_dxf)
{
    projection()
    {
        difference()
        {
            hull()
            {
                translate([-length/2+width/2,0,height/2])
                cylinder(h=height,d=width,center=true);
                translate([length/2-width/2,0,height/2])
                cylinder(h=height,d=width,center=true);
            }

            translate([-length/2+(hole_d*2),0,height/2])
            cylinder(h=height,d=hole_d,center=true);
            translate([length/2-(hole_d*2),0,height/2])
            cylinder(h=height,d=hole_d,center=true);
            translate([-length/2+hole_d,0,height/2])
            cube([shaft_l,shaft_w,height],center=true);
            translate([length/2-hole_d,0,height/2])
            cube([shaft_l,shaft_w,height],center=true);
        }
    }
}
else{
    difference()
    {
        hull()
        {
            translate([-length/2+width/2,0,height/2])
            cylinder(h=height,d=width,center=true);
            translate([length/2-width/2,0,height/2])
            cylinder(h=height,d=width,center=true);
        }
        translate([-length/2+(hole_d*2),0,height/2])
        cylinder(h=height,d=hole_d,center=true);
        translate([length/2-(hole_d*2),0,height/2])
        cylinder(h=height,d=hole_d,center=true);
        translate([-length/2+hole_d,0,height/2])
        cube([shaft_l,shaft_w,height],center=true);
        translate([length/2-hole_d,0,height/2])
        cube([shaft_l,shaft_w,height],center=true);
    }
}